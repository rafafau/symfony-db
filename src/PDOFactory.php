<?php

declare(strict_types=1);

namespace App;

class PDOFactory
{
    private $host;
    private $port;
    private $username;
    private $password;
    private $databaseName;

    public function __construct(string $host, string $port, string $username, string $password, string $databaseName)
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->databaseName = $databaseName;
    }

    public function create(): \PDO
    {
        $dsn = "mysql:host={$this->host};port={$this->port};dbname={$this->databaseName}";

        return new \PDO($dsn, $this->username, $this->password);
    }
}