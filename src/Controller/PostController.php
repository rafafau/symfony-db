<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/posts/", name = "get_posts", methods = {"GET"})
     */
    public function getPostsAction(): JsonResponse
    {
        $posts = $this->postRepository->getAllPosts();

        return $this->json($posts);
    }


    /**
     * @Route("/posts/{id}", name = "get_post", methods = {"GET"})
     */
    public function getPostAction(int $id): JsonResponse
    {
        $post = $this->postRepository->getPostById($id);

        return $this->json($post);
    }


    /**
     * @Route("/posts_recent", name = "get_recent_posts", methods = {"GET"})
     */
    public function getRecentPostsAction(): JsonResponse
    {
        $posts = $this->postRepository->getRecentPosts();

        return $this->json($posts);
    }


    /**
     * @Route("/posts_recent_short", name = "get_recent_posts_short", methods = {"GET"})
     */
    public function getRecentPostsShortAction(): JsonResponse
    {
        $posts = $this->postRepository->getRecentPostsWithTitleAndHeadline();

        return $this->json($posts);
    }

    /**
     * @Route("/posts/from/{from}/to/{to}", name = "posts", methods = {"GET"})
     */
    public function getPostsByDatesAction(string $from, string $to): JsonResponse
    {
        $posts = $this->postRepository->getPostByDate($from, $to);

        return $this->json($posts);
    }

    /**
     * @Route("/posts_users/{userId}", name = "posts", methods = {"GET"})
     */
    public function getUserPosts(int $userId): JsonResponse
    {
        $posts = $this->postRepository->getPostsByUser($userId);

        return $this->json($posts);
    }
}