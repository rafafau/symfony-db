<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/users", name = "get_users", methods = {"GET"})
     */
    public function getUsersAction(): JsonResponse
    {
        $users = $this->userRepository->getAllUsers();

        return $this->json($users);
    }

    /**
     * @Route("/users/{id}", name = "get_user", methods = {"GET"})
     */
    public function getUserAction(int $id): JsonResponse
    {
        $user = $this->userRepository->getUserById($id);

        if (!$user) {
            throw $this->createNotFoundException("User not found, ID: $id");
        }

        return $this->json($user);
    }

    /**
     * @Route("/users_by_date/{from}/{to}", name = "get_users_by_date", methods = {"GET"})
     */
    public function getUserByDate(string $from, string $to): JsonResponse
    {
        $users = $this->userRepository->getUsersByDate($from, $to);

        return $this->json($users);

    }

    /**
     * @Route("/users_emails", name = "get_users_emails", methods = {"GET"})
     */
    public function getUsersEmails(): JsonResponse
    {
        $users = $this->userRepository->getUsersEmails();

        return $this->json($users);

    }

}