<?php

declare(strict_types=1);

namespace App\Repository;

use App\PDOFactory;

class PostRepository
{
    /**
     * @var \PDO
     */
    private $pdo;

    public function __construct(PDOFactory $pdoFactory)
    {
        $this->pdo = $pdoFactory->create();
    }

    /**
     * Zwraca wszystkie posty użytkowników
     */
    public function getAllPosts(): array
    {

    }

    /**
     * Zwraca jeden post (lub null jesli taki nie istnieje) o podanym ID
     */
    public function getPostById(int $id):? array
    {

    }

    /**
     * Zwraca 10 najnowszych postów
     */
    public function getRecentPosts(): array
    {

    }

    /**
     * Zwraca 10 najnowszych postów ale tylko z tytułem i headlinem
     */
    public function getRecentPostsWithTitleAndHeadline(): array
    {

    }

    /**
     * Zwraca wszystkie posty danego użytkownika
     */
    public function getPostsByUser(int $userId): array
    {

    }
}