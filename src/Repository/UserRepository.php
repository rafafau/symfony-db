<?php

declare(strict_types=1);

namespace App\Repository;

use App\PDOFactory;

class UserRepository
{
    /**
     * @var \PDO
     */
    private $pdo;

    public function __construct(PDOFactory $pdoFactory)
    {
        $this->pdo = $pdoFactory->create();
    }

    public function getAllUsers(): array
    {
        $query = $this->pdo->query('SELECT * FROM users');

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getUserById(int $id):? array
    {
        $query = $this->pdo->prepare('SELECT * FROM users WHERE id = :userId');

        $query->bindValue(':userId', $id, \PDO::PARAM_INT);

        $query->execute();

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        if (is_bool($result)) {
            return null;
        }

        return $result;
    }

    public function getUsersByDate(string $from, string $to):? array
    {
        $query = $this->pdo->prepare('SELECT * FROM users WHERE created_at > :from AND created_at < :to');

        $query->bindValue(':from', $from);
        $query->bindValue(':to', $to);

        $query->execute();

        $results = [];

        while($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $user = $row;

            $user['address'] = '*****';

            $results[] = $user;
        }

        return $results;
    }

    public function getUsersEmails(): array
    {
        $query = $this->pdo->prepare('SELECT email FROM users');

        $query->execute();

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}